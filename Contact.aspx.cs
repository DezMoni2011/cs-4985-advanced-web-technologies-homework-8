﻿using System;
using System.Web.UI;

/// <summary>
/// This is the code-behind file for the Contact page
/// </summary>
/// <author>
/// Destiny Harris
/// </author>
/// <version>
/// Febuary 3, 2015 | Spring
/// </version>
public partial class Request : Page
{
    private ContactDetails _contactDetails;

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        this._contactDetails = new ContactDetails();

        if (IsPostBack)
        {
            return;
        }

        var firstNameCookie = Request.Cookies["FirstName"];
        var firsNameSession = Session["FirstName"];

        if (firstNameCookie != null)
        {
            this._contactDetails.LastName = firstNameCookie.Value;
        }
        else if (firsNameSession != null)
        {
            this._contactDetails.FirstName = firsNameSession.ToString();
        }

        var lastNameCookie = Request.Cookies["LastName"];
        var lastNameSession = Session["LastName"];

        if (lastNameCookie != null)
        {
            this._contactDetails.LastName = lastNameCookie.Value;
        }
        else if (lastNameSession != null)
        {
            this._contactDetails.LastName = lastNameSession.ToString();
        }

        var emailSession = Session["Email"];
        if (emailSession != null)
        {
            this._contactDetails.Email = emailSession.ToString();
        }

        var phoneSession = Session["Phone"];
        if (phoneSession != null)
        {
            this._contactDetails.Phone = phoneSession.ToString();
        }

        var prefererdMethodSession = Session["PreferredMethod"];
        if (prefererdMethodSession != null)
        {
            this._contactDetails.PerfferedMethod = prefererdMethodSession.ToString();
        }

        this.DisplayReservation();
    }

    

    /// <summary>
    /// Displays the reservation.
    /// </summary>
    private void DisplayReservation()
    {
        this.txtFirstName.Text = this._contactDetails.FirstName;
        this.txtLastName.Text = this._contactDetails.LastName;
        this.txtEmail.Text = this._contactDetails.Email;
        this.txtPhone.Text = this._contactDetails.Phone;
        this.ddlPreferredMethod.SelectedValue = this._contactDetails.PerfferedMethod;
    }

    /// <summary>
    /// Handles the Click event of the btnClear control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnClear_Click(object sender, EventArgs e)
    {
        this.txtFirstName.Text = "";
        this.txtLastName.Text = "";
        this.txtEmail.Text = "";
        this.txtPhone.Text = "";
        this.ddlPreferredMethod.SelectedIndex = 0;
        this.lblMessage.Text = "";
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        Session["LastName"] = this.txtLastName.Text;
        Session["FirstName"] = this.txtFirstName.Text;
        Session["Email"] = this.txtEmail.Text;
        Session["Phone"] = this.txtPhone.Text;
        Session["PrefferedMethod"] = this.ddlPreferredMethod.SelectedIndex;
        Response.Redirect("/Confirm.aspx");
    }


}
