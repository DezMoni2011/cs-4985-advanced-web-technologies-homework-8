﻿using System;
using System.Diagnostics;

/// <summary>
/// Represents the contact details object.
/// </summary>
/// <author>
/// Destiny Harris
/// </author>
/// <version>
/// Feburary 3, 2015 | Spring
/// </version>
public class ContactDetails
{
    /// <summary>
    /// The first name
    /// </summary>
    private String _firstName;
    /// <summary>
    /// The last name
    /// </summary>
    private String _lastName;
    /// <summary>
    /// The email
    /// </summary>
    private String _email;
    /// <summary>
    /// The phone number
    /// </summary>
    private String _phoneNumber;
    /// <summary>
    /// The perffered method of contact
    /// </summary>
    private String _prefferedMethodOfContact;

    /// <summary>
    /// Gets or sets the first name.
    /// </summary>
    /// <value>
    /// The first name.
    /// </value>
    public String FirstName
    {
        get { return this._firstName; }
        set
        {
            Trace.Assert(value != null, "First name must not be null");
            this._firstName = value;
        }
    }

    /// <summary>
    /// Gets or sets the last name.
    /// </summary>
    /// <value>
    /// The last name.
    /// </value>
    public String LastName
    {
        get { return this._lastName; }
        set
        {
            Trace.Assert(value != null, "Last name must not be null.");
            this._lastName = value;
        }
    }

    /// <summary>
    /// Gets or sets the email.
    /// </summary>
    /// <value>
    /// The email.
    /// </value>
    public String Email
    {
        get { return this._email; }
        set
        {
            Trace.Assert(value != null, "Email must not be null.");
            this._email = value;
        }
    }

    /// <summary>
    /// Gets or sets the phone.
    /// </summary>
    /// <value>
    /// The phone.
    /// </value>
    public String Phone
    {
        get { return this._phoneNumber; }
        set
        {
            Trace.Assert(value != null, "Phone number must not be null.");
            this._phoneNumber = value;
        }
    }

    /// <summary>
    /// Gets or sets the perffered method.
    /// </summary>
    /// <value>
    /// The perffered method.
    /// </value>
    public String PerfferedMethod
    {
        get { return this._prefferedMethodOfContact; }
        set
        {
            Trace.Assert(value != null, "Perferred method must not be null.");
            this._prefferedMethodOfContact = value;
        }
    }
}