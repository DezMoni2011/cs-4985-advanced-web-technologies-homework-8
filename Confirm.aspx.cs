﻿using System;
using System.Web;
using System.Web.UI;

/// <summary>
/// This is the code-behind file for the Confirmation page
/// </summary>
/// <author>
/// Destiny Harris
/// </author>
/// <version>
/// Febuary 3, 2015 | Spring 
/// </version>
public partial class Confirm : Page
{
    private ContactDetails _confirmedContactDetails;

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        this._confirmedContactDetails = new ContactDetails();

        var firstNameCookie = Request.Cookies["FirstName"];
        var firsNameSession = Session["FirstName"];

        if (firstNameCookie != null)
        {
            this._confirmedContactDetails.LastName = firstNameCookie.Value;
        }
        else if (firsNameSession != null)
        {
            this._confirmedContactDetails.FirstName = firsNameSession.ToString();
        }

        var lastNameCookie = Request.Cookies["LastName"];
        var lastNameSession = Session["LastName"];

        if (lastNameCookie != null)
        {
            this._confirmedContactDetails.LastName = lastNameCookie.Value;
        }
        else if (lastNameSession != null)
        {
            this._confirmedContactDetails.LastName = lastNameSession.ToString();
        }

        var emailSession = Session["Email"];
        if (emailSession != null)
        {
            this._confirmedContactDetails.Email = emailSession.ToString();
        }

        var phoneSession = Session["Phone"];
        if (phoneSession != null)
        {
            this._confirmedContactDetails.Phone = phoneSession.ToString();
        }

        var prefererdMethodSession = Session["PreferredMethod"];
        if (prefererdMethodSession != null)
        {
            this._confirmedContactDetails.PerfferedMethod = prefererdMethodSession.ToString();
        }

        this.DisplayReservation();
    }

    /// <summary>
    /// Displays the reservation.
    /// </summary>
    private void DisplayReservation()
    {
        this.lblFirstName.Text = this._confirmedContactDetails.FirstName;
        this.lblLastName.Text = this._confirmedContactDetails.LastName;
        this.lblEmail.Text = this._confirmedContactDetails.Email;
        this.lblPhone.Text = this._confirmedContactDetails.Phone;
        this.lblPreferredMethod.Text = this._confirmedContactDetails.PerfferedMethod;
    }

    protected void btnConfirm_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            this.SetResponseCookie("LastName", this.lblLastName.Text);
            this.SetResponseCookie("FirstName", this.lblFirstName.Text);
        }
    }

    private void SetResponseCookie(String cookieName, String cookieValue)
    {
        var cookieToAdd = new HttpCookie(cookieName);
        cookieToAdd.Value = cookieValue;
        cookieToAdd.Expires = DateTime.Now.AddSeconds(15);
        Response.Cookies.Add(cookieToAdd);
    }
}